var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám đa khoa trung tâm y tế thành phố Rạch Giá",
   "address": "Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 9.9943793,
   "Latitude": 105.0943227
 },
 {
   "STT": 2,
   "Name": "Phòng khám khu vực Hòn Tre",
   "address": "Xã Hòn Tre, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.96574839999999,
   "Latitude": 104.8448719
 },
 {
   "STT": 3,
   "Name": "Phòng Khám đa khoa khu vực An Thới",
   "address": "Thị trấn An Thới , Huyện Phú Quốc, Tỉnh Kiên Giang",
   "Longtitude": 10.042699,
   "Latitude": 104.0130391
 },
 {
   "STT": 4,
   "Name": "Phòng khám đa khoa Thanh Bình",
   "address": "thị trấn Minh Lương, huyện Châu Thành, tỉnh Kiên Giang",
   "Longtitude": 9.9044207,
   "Latitude": 105.1668326
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa CN CT CP Phòng khám đa khoa Duy khang Rạch Giá",
   "address": "11-12 B5 Nguyễn Phương Danh_Vĩnh bảo-RG",
   "Longtitude": 9.9999658,
   "Latitude": 105.08475
 },
 {
   "STT": 6,
   "Name": "Doanh nghiệp tư nhân Phòng khám đa khoa Mỹ Hạnh",
   "address": " Thị trấn Kiên Lương, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.2456707,
   "Latitude": 104.6063192
 },
 {
   "STT": 7,
   "Name": "Công Ty TNHH B V An Hòa - Phòng Khám Đa Khoa An Hòa",
   "address": "Đường Phan Thị Ràng, Thành phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 9.9676194,
   "Latitude": 105.1005002
 },
 {
   "STT": 8,
   "Name": "CÔNG TY TNHH MỘT THÀNH VIÊN PHÒNG KHÁM ĐA KHOA NHÂN HẬU PHÚ CƯỜNG",
   "address": "Phường An Hòa, Thành phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 9.9804585,
   "Latitude": 105.1043935
 },
 {
   "STT": 10,
   "Name": "Công ty TNHH một thành viên phòng khám đa khoa Bình Dương",
   "address": "Thị trấn Giồng Riềng, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8984837,
   "Latitude": 105.2955575
 },
 {
   "STT": 11,
   "Name": "DOANH NGHIỆP TƯ NHÂN PHÒNG KHÁM ĐA KHOA VẠN PHƯỚC SÓC XOÀI",
   "address": "Thị trấn Sóc Sơn, Huyện Hòn Đất, tỉnh Kiên Giang",
   "Longtitude": 10.1070034,
   "Latitude": 105.0148322
 },
 {
   "STT": 12,
   "Name": "Phòng Khám Đa Khoa Trung Trực",
   "address": "Đường Nguyễn Trung Trực, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Kiên Giang.",
   "Longtitude": 9.9915051,
   "Latitude": 105.0964663
 },
 {
   "STT": 13,
   "Name": "Nha khoa Việt Mỹ - Chi nhánh Rạch Giá 2",
   "address": "Đường Nguyễn Trung Trực, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Kiên Giang.",
   "Longtitude": 9.9915051,
   "Latitude": 105.0964663
 },
 {
   "STT": 14,
   "Name": "Phòng khám Mắt - Bác sĩ Phạm Hoàng Tuấn",
   "address": "Phường Vĩnh Quang, Thành phố Rạch Giá, Tỉnh Kiên Giang ",
   "Longtitude": 10.0260579,
   "Latitude": 105.0690984
 },
 {
   "STT": 15,
   "Name": "Phòng khám Nhi khoa - Bác sĩ Danh Tý",
   "address": "Phường Vĩnh Bảo, Thành phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 10.0030212,
   "Latitude": 105.0863077
 },
 {
   "STT": 16,
   "Name": "Phòng khám Bác sĩ Phùng Thị Tuyết Phiêu",
   "address": "Phường Vĩnh Thanh Vân, Thành phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 10.0095159,
   "Latitude": 105.0832318
 }
];