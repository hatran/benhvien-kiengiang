var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện đa khoa tỉnh Kiên Giang",
   "address": "Phường Vĩnh Thanh Vân,TP Rạch Giá, Kiên Giang",
   "Longtitude": 10.0095159,
   "Latitude": 105.0832318,
   "Number_of_beds": "Đặng Thị Minh",
    "area": "02033.819.115",
    "laixe": "Bùi Đức Toàn",
    "bacsi": "Nông Văn Dũng",
    "dieuduong": "Phạm Thái Dương"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Y dược cổ truyền tỉnh Kiên Giang",
   "address": "Phường Vĩnh Lạc, TP Rạch Giá, Kiên Giang",
   "Longtitude": 9.9943793,
   "Latitude": 105.0943227,
   "Number_of_beds": " Nguyễn Đức Bằng",
    "area": "02033.670.297",
    "laixe": "Nguyễn Văn Sinh",
    "bacsi": "Nguyễn Văn Hùng",
    "dieuduong": "Phạm Thế Ninh"
 },
 {
   "STT": 3,
   "Name": "Bệnh viện tư nhân Bình An",
   "address": "Nguyễn Trung Trực, Phường Vĩnh Bảo, tp. Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.0045545,
   "Latitude": 105.0844343,
   "Number_of_beds": "Vũ Duy Điều",
    "area": "0359.008.555",
    "laixe": "Ngô Quang Huynh",
    "bacsi": "Nguyễn Đức Cử",
    "dieuduong": "Nguyễn Thị Khánh"
 },
 {
   "STT": 4,
   "Name": "Bệnh viện huyện An Minh",
   "address": "TT. Thứ Mười Một, An Minh, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.7134832,
   "Latitude": 104.9647228,
   "Number_of_beds": "Tô Lê Hoàng An",
    "area": "02033.862.245",
    "laixe": "Tô Tuấn Dũng",
    "bacsi": "Chu Văn Tước",
    "dieuduong": "Trần T. Hương Thảo"
 },
 {
   "STT": 5,
   "Name": "Bệnh viện huyện An Biên",
   "address": "TT. Thứ Ba, tx.An Biên, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.817606,
   "Latitude": 105.0636672,
   "Number_of_beds": "Triệu Đức Đường",
    "area": "0966.271.313",
    "laixe": "Ma Khánh Dân",
    "bacsi": "Triệu Đức Đường",
    "dieuduong": "Phạm Thị Thảo"
 },
 {
   "STT": 6,
   "Name": "Bệnh viện huyện Vinh Thuận",
   "address": "Vĩnh Thuận, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.5106455,
   "Latitude": 105.2582522,
   "Number_of_beds": "Nguyễn Khắc Mạnh",
    "area": "0886.891.685",
    "laixe": " Phạm Quốc Vương",
    "bacsi": "Nguyễn Huy Dương",
    "dieuduong": "Lê Văn Thắng"
 },
 {
   "STT": 7,
   "Name": "Bệnh viện huyện Giồng Riềng",
   "address": "TT. Giồng Riềng, Giồng Riềng, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.9077243,
   "Latitude": 105.3084359,
   "Number_of_beds": "Nguyễn Khắc Mạnh",
    "area": "0886.891.685",
    "laixe": " Phạm Quốc Vương",
    "bacsi": "Nguyễn Huy Dương",
    "dieuduong": "Lê Văn Thắng"
 },
 {
   "STT": 8,
   "Name": "Bệnh viện huyện Gò Quao",
   "address": "TT. Gò Quao, Gò Quao, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.7402775,
   "Latitude": 105.2750797,
   "Number_of_beds": "Phạm Tiến Dũng",
    "area": "0869.355.386",
    "laixe": "Lương Văn Long",
    "bacsi": "Hoàng Văn Hùng",
    "dieuduong": "Nguyễn Văn Đức"
 },
 {
   "STT": 9,
   "Name": "Bệnh viện huyện Châu Thành",
   "address": "TT. Minh Lương, Châu Thành, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.0669087,
   "Latitude": 105.4458995,
   "Number_of_beds": "Khúc Thị Miền",
    "area": "02033.503.030",
    "laixe": "Nguyễn Văn Lâm",
    "bacsi": "Khúc Thị Miền",
    "dieuduong": "Hoàng Thị Yến"
 },
 {
   "STT": 10,
   "Name": "Bệnh viện huyện Tân Hiệp",
   "address": "TT. Tân Hiệp, Tân Hịêp, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1132946,
   "Latitude": 105.2810252,
   "Number_of_beds": "Khúc Thị Miền",
    "area": "02033.503.030",
    "laixe": "Nguyễn Văn Lâm",
    "bacsi": "Khúc Thị Miền",
    "dieuduong": "Hoàng Thị Yến"
 },
 {
   "STT": 11,
   "Name": "Bệnh viện huyện Hòn Đất",
   "address": "TT. Hòn Đất, Hòn Đất, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1791336,
   "Latitude": 104.9371335,
   "Number_of_beds": "Lại Ngọc Dương",
    "area": "02033.742.611",
    "laixe": "Đinh Văn Phước",
    "bacsi": "Lục Thành Huy",
    "dieuduong": " Nguyễn Tuấn Hải"
 },
 {
   "STT": 12,
   "Name": "Bệnh viện huyện Kiên Lương",
   "address": "Thị Trấn Kiên Lương, Huyện Kiên Lương, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.2517428,
   "Latitude": 104.5956464,
   "Number_of_beds": "Hoàng Bích Hằng",
    "area": "02033.878.244",
    "laixe": "Lý Đức Thiện",
    "bacsi": "Hoàng Thúy Vân",
    "dieuduong": "Ngô Thị Mai"
 },
 {
   "STT": 13,
   "Name": "Bệnh viện thị xã Hà Tiên",
   "address": "Khu Phố 3, Hà Tiên, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.37292,
   "Latitude": 104.488873,
   "Number_of_beds": "Phạm Đức Thịnh",
    "area": "02033.768.115",
    "laixe": "Nguyễn Anh Tuấn",
    "bacsi": "Phạm Đức Thịnh",
    "dieuduong": "Hoàng Đức Tâm"
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Phú Quốc",
   "address": "TT. Dương Đông, Phú Quốc, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1561993,
   "Latitude": 103.8023344,
   "Number_of_beds": "Phạm Thanh Tùng",
    "area": "0966.311.313",
    "laixe": "Lương Vĩnh Thanh",
    "bacsi": "Nguyễn Tuấn Anh",
    "dieuduong": "Phạm Việt Anh"
 }
];