var datatramyte = [
 {
   "STT": 13,
   "Name": "Trạm y tế Xã Vĩnh Thành A",
   "address": "Xã Vĩnh Thành A, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.9276951,
   "Latitude": 105.1401271
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Xã Tân Thạch",
   "address": "Xã Tân Thạch, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.66471829999999,
   "Latitude": 104.9495651
 },
 {
   "STT": 15,
   "Name": "Trạm xá bưu điện Tỉnh Kiên Giang",
   "address": "Phường An Hòa, Thành phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9804585,
   "Latitude": 105.1043935
 },
 {
   "STT": 16,
   "Name": "Trạm xá công ty xi măng Hà Tiên 2",
   "address": "Thị Trấn Kiên Lương, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.2456707,
   "Latitude": 104.6063192
 },
 {
   "STT": 17,
   "Name": "Trạm Y tế Xã Tân Thạnh",
   "address": "Xã Tân Thạnh, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.71570749999999,
   "Latitude": 104.9038451
 },
 {
   "STT": 18,
   "Name": "Trạm y tế TT Kiên Lương",
   "address": "Thị Trấn Kiên Lương, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.2456707,
   "Latitude": 104.6063192
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Thị trấn Hòn Đất",
   "address": "Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2227003,
   "Latitude": 104.9856176
 },
 {
   "STT": 20,
   "Name": "Trạm y tế thị trấn Tân Hiệp",
   "address": "Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1154358,
   "Latitude": 105.2834404
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Thị Trấn Minh Lương",
   "address": "Thị Trấn Minh Lương, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.9044207,
   "Latitude": 105.1668326
 },
 {
   "STT": 22,
   "Name": "Trạm y tế thị trấn Giồng Riềng",
   "address": "Thị Trấn Giồng Riềng, Huyện Giồng Riềng, Tỉnh  Kiên Giang",
   "Longtitude": 9.8984837,
   "Latitude": 105.2955575
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Thị trấn Gò Quao",
   "address": "Thị Trấn Gò quao, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.74789659999999,
   "Latitude": 105.2721456
 },
 {
   "STT": 24,
   "Name": "Trạm y tế thị trấn Thứ 3",
   "address": "Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.8064559,
   "Latitude": 105.0557415
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Thị trấn Thứ 11",
   "address": "Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.66471829999999,
   "Latitude": 104.9495651
 },
 {
   "STT": 26,
   "Name": "Trạm y tế thị trấn Vĩnh  thuận",
   "address": "Thị Trấn Vĩnh Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.51064549999999,
   "Latitude": 105.2604409
 },
 {
   "STT": 27,
   "Name": "Trạm y tế thị trấn Dương Đông",
   "address": "Huyện Phú Quốc, Tỉnh Kiên Giang",
   "Longtitude": 10.289879,
   "Latitude": 103.98402
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Xã Bình Trị",
   "address": "Xã Bình Trị, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.1805016,
   "Latitude": 104.6592079
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Phong Đông",
   "address": "Xã Phong Đông, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.5250089,
   "Latitude": 105.3014109
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Sơn Bình",
   "address": "Xã Thổ Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang ",
   "Longtitude": 10.1284592,
   "Latitude": 104.9038451
 },
 {
   "STT": 31,
   "Name": "Trạm xá khu vực Rạch Sỏi",
   "address": "Phường Rạch Sỏi, Thành Phố Rạch Giá, Tỉnh Kiên Giang",
   "Longtitude": 9.9483946,
   "Latitude": 105.1156893
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Xã Thạnh Bình",
   "address": "Xã Thạnh Bình, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.9325637,
   "Latitude": 105.3056586
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1743547,
   "Latitude": 105.2065457
 },
 {
   "STT": 36,
   "Name": "Trạm y tế phường Vĩnh Lợi",
   "address": "Phường Vĩnh Lợi, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9552245,
   "Latitude": 105.1236923
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Phi Thông",
   "address": "Xã Phi Thông, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0549515,
   "Latitude": 105.1211419
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Phường An Hòa",
   "address": "Phường An Hòa, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9804585,
   "Latitude": 105.1043935
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Phường Vĩnh Quang",
   "address": "Phường Vĩnh Quang, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0260579,
   "Latitude": 105.0690984
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Phường Vĩnh Hiệp",
   "address": "Phường Vĩnh Hiệp, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9917553,
   "Latitude": 105.1043574
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Phường Vĩnh Lạc",
   "address": "Phường Vĩnh Lạc, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9943793,
   "Latitude": 105.0943227
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Phường Vĩnh Thanh",
   "address": "Phường Vĩnh Thanh, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0145123,
   "Latitude": 105.0818276
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Phường Vĩnh Thanh Vân",
   "address": "Phường Vĩnh Thanh, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0145123,
   "Latitude": 105.0818276
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Phường Vĩnh Thông",
   "address": "Phường Vĩnh Thông,  Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0361694,
   "Latitude": 105.0953774
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Phường Rạch Sỏi",
   "address": "Phường Rạch Sỏi, Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9483946,
   "Latitude": 105.1156893
 },
 {
   "STT": 46,
   "Name": "Trạm y tế phường An Bình",
   "address": "Phường An Hòa,  Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 9.9804585,
   "Latitude": 105.1043935
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Phường Vĩnh bảo",
   "address": "Phường Vĩnh Bảo,  Thành Phố Rạch Gía, Tỉnh Kiên Giang",
   "Longtitude": 10.0030212,
   "Latitude": 105.0863077
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã An Sơn",
   "address": "Xã An Sơn, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.6904228,
   "Latitude": 104.3617343
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Nam Du",
   "address": "Xã Nam Du, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.6729085,
   "Latitude": 104.3908304
 },
 {
   "STT": 50,
   "Name": "Trạm y tế phường Bình San",
   "address": "Phường Bình San, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.390722,
   "Latitude": 104.4810648
 },
 {
   "STT": 51,
   "Name": "Trạm y tế phường Đông Hồ",
   "address": "Phường Đông Hồ, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.4029259,
   "Latitude": 104.5130967
 },
 {
   "STT": 52,
   "Name": "Trạm y tế phường Pháo Đài",
   "address": "Phường Pháo Đài, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3837166,
   "Latitude": 104.4548621
 },
 {
   "STT": 53,
   "Name": "Trạm y tế phường Tô Châu",
   "address": "Phường Tô Châu, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3735151,
   "Latitude": 104.4985359
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Mỹ Đức",
   "address": "Xã Mỹ Đức, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.4160066,
   "Latitude": 104.4665072
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Thuận Yên",
   "address": "Xã Thuận Yên, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3471797,
   "Latitude": 104.5363969
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Tiên Hải",
   "address": "Xã Tiên Hải, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.2910916,
   "Latitude": 104.3268266
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Bình Minh",
   "address": "Xã Bình Minh, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.5908511,
   "Latitude": 105.213961
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Vĩnh Phong",
   "address": "Xã Vĩnh Phong, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.4407836,
   "Latitude": 105.2545888
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Vĩnh Thuận",
   "address": "Xã Vĩnh Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.4870331,
   "Latitude": 105.1843801
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Vĩnh Hòa",
   "address": "Xã Vĩnh Hòa, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.68153319999999,
   "Latitude": 105.1843801
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Vĩnh Bình Bắc",
   "address": "Xã Vĩnh Bình Bắc, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.6361706,
   "Latitude": 105.2311827
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Vĩnh Bình Nam",
   "address": "Xã Vĩnh Bình Nam, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.5702876,
   "Latitude": 105.2545888
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Minh Thuận",
   "address": "Xã Minh Thuận, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.58578629999999,
   "Latitude": 105.1492869
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Tân Thuận",
   "address": "Xã Tân Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.52917699999999,
   "Latitude": 105.2077798
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Hòa Chánh",
   "address": "Xã Hòa Chánh, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.6879423,
   "Latitude": 105.221768
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Bình Sơn",
   "address": "Xã Bình Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.261398,
   "Latitude": 104.8571368
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Bình Giang",
   "address": "Xã Bình Giang, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.3083013,
   "Latitude": 104.7871004
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Mỹ Hiệp Sơn",
   "address": "Xã Mỹ Hiệp Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2161843,
   "Latitude": 105.0955882
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Mỹ Lâm",
   "address": "Xã Mỹ Lâm,  Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.0567029,
   "Latitude": 105.044052
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Nam Thái Sơn",
   "address": "Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2227003,
   "Latitude": 104.9856176
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Sơn Kiên",
   "address": "Xã Sơn Kiên, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1728606,
   "Latitude": 105.0031457
 },
 {
   "STT": 72,
   "Name": "Trạm y tế thị trấn Sóc Sơn",
   "address": "Thị Trấn Sóc Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1070034,
   "Latitude": 105.0148322
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã Thổ Sơn",
   "address": "Xã Thổ Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1284592,
   "Latitude": 104.9038451
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã Mỹ Phước",
   "address": " Xã Mỹ Phước, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1350635,
   "Latitude": 105.100493
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Nam Thái",
   "address": "Xã Nam Thái, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.25704,
   "Latitude": 104.9505671
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Mỹ thuận",
   "address": "Xã Mỹ Thuận, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1437361,
   "Latitude": 105.044052
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Lình Huỳnh",
   "address": "Xã Lình Huỳnh, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1523925,
   "Latitude": 104.8571368
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Mỹ Thái",
   "address": "Xã Mỹ Thái, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2272864,
   "Latitude": 105.0298969
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Xã Nam Du",
   "address": "Xã Nam Du, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.6729085,
   "Latitude": 104.3908304
 },
 {
   "STT": 80,
   "Name": "Trạm y tế phường Bình San",
   "address": "Phường Bình San, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.390722,
   "Latitude": 104.4810648
 },
 {
   "STT": 81,
   "Name": "Trạm y tế phường Đông Hồ",
   "address": "Phường Đông Hồ, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.4029259,
   "Latitude": 104.5130967
 },
 {
   "STT": 82,
   "Name": "Trạm y tế phường Pháo Đài",
   "address": "Phường Pháo Đài, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3837166,
   "Latitude": 104.4548621
 },
 {
   "STT": 83,
   "Name": "Trạm y tế phường Tô Châu",
   "address": "Phường Tô Châu, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3735151,
   "Latitude": 104.4985359
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Mỹ Đức",
   "address": "Xã Mỹ Đức, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.4160066,
   "Latitude": 104.4665072
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Thuận Yên",
   "address": "Xã Thuận Yên, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3471797,
   "Latitude": 104.5363969
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Tiên Hải",
   "address": "Xã Tiên Hải, Thị Xã Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.2910916,
   "Latitude": 104.3268266
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Bình Minh",
   "address": "Xã Bình Minh, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.5908511,
   "Latitude": 105.213961
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Vĩnh Phong",
   "address": "Xã Vĩnh Phong, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.4407836,
   "Latitude": 105.2545888
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Vĩnh Thuận",
   "address": "Xã Vĩnh Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.4870331,
   "Latitude": 105.1843801
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Vĩnh Hòa",
   "address": "Xã Vĩnh Hòa, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.68153319999999,
   "Latitude": 105.1843801
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Vĩnh Bình Bắc",
   "address": "Xã Vĩnh Bình Bắc, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.6361706,
   "Latitude": 105.2311827
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Vĩnh Bình Nam",
   "address": "Xã Vĩnh Bình Nam, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.5702876,
   "Latitude": 105.2545888
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Minh Thuận",
   "address": "Xã Minh Thuận, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.58578629999999,
   "Latitude": 105.1492869
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Tân Thuận",
   "address": "Xã Tân Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.52917699999999,
   "Latitude": 105.2077798
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Hòa Chánh",
   "address": "Xã Hòa Chánh, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.6879423,
   "Latitude": 105.221768
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Bình Sơn",
   "address": "Xã Bình Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.261398,
   "Latitude": 104.8571368
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Bình Giang",
   "address": "Xã Bình Giang, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.3083013,
   "Latitude": 104.7871004
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Mỹ Hiệp Sơn",
   "address": "Xã Mỹ Hiệp Sơn, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2161843,
   "Latitude": 105.0955882
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã Mỹ Lâm",
   "address": "Xã Mỹ Lâm, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.0567029,
   "Latitude": 105.044052
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã Nam Thái Sơn",
   "address": "Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2227003,
   "Latitude": 104.9856176
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Sơn Kiên",
   "address": "Xã Sơn Kiên, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1728606,
   "Latitude": 105.0031457
 },
 {
   "STT": 102,
   "Name": "Trạm y tế thị trấn Sóc Sơn",
   "address": "Thị Trấn Sóc Sơn,  Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1070034,
   "Latitude": 105.0148322
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Thổ Sơn",
   "address": "Xã Thổ Sơn,  Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1284592,
   "Latitude": 104.9038451
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Mỹ Phước",
   "address": "Xã Mỹ Phước, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1350635,
   "Latitude": 105.100493
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Nam Thái",
   "address": "Xã Nam Thái, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.25704,
   "Latitude": 104.9505671
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Mỹ thuận",
   "address": "Xã Mỹ Thuận, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1437361,
   "Latitude": 105.044052
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Lình Huỳnh",
   "address": "Xã Lình Huỳnh, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1523925,
   "Latitude": 104.8571368
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Mỹ Thái",
   "address": "Xã Mỹ Thái, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.2272864,
   "Latitude": 105.0298969
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Bãi Thơm",
   "address": "Xã Bãi Thơm, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.3879375,
   "Latitude": 103.9956269
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Xã Cửa Cạn",
   "address": " Xã Cửa Cạn, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.3043743,
   "Latitude": 103.902799
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Xã Cửa Dương",
   "address": "Xã Cửa Dương, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.289879,
   "Latitude": 103.98402
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Xã Dương Tơ",
   "address": "Xã Dương Tơ, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.1471942,
   "Latitude": 103.9956269
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Xã Gành Dầu",
   "address": "Xã Gành Dầu, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.34819,
   "Latitude": 103.902799
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Xã Hàm Ninh",
   "address": "Xã Hàm Ninh, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 10.2107621,
   "Latitude": 104.0420642
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Xã Thổ Châu",
   "address": "Xã Thổ Châu, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 9.3134297,
   "Latitude": 103.4916438
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Xã Hòn Thơm",
   "address": "Xã Hòn Thơm, Huyện Phú Quốc,  Tỉnh Kiên Giang ",
   "Longtitude": 9.9279383,
   "Latitude": 104.0188436
 },
 {
   "STT": 117,
   "Name": "Trạm Y tế Xã Tân Hiệp A",
   "address": "Xã Tân Hiệp A, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.065613,
   "Latitude": 105.231535
 },
 {
   "STT": 118,
   "Name": "Trạm Y tế Xã Tân Hiệp B",
   "address": "Xã Tân Hiệp B, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1487334,
   "Latitude": 105.2447882
 },
 {
   "STT": 119,
   "Name": "Trạm Y tế Xã Tân Hội",
   "address": "Xã Tân Hội, , Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1142204,
   "Latitude": 105.1516272
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Xã Thạnh Đông",
   "address": "Xã Thạnh Đông, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.0500784,
   "Latitude": 105.346183
 },
 {
   "STT": 121,
   "Name": "Trạm Y tế Xã Thạnh Đông A",
   "address": "Xã Thạnh Đông A, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.0500784,
   "Latitude": 105.346183
 },
 {
   "STT": 122,
   "Name": "Trạm Y tế Xã Thạnh Đông B",
   "address": "Xã Thạnh Đông B, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.065631,
   "Latitude": 105.348108
 },
 {
   "STT": 123,
   "Name": "Trạm Y tế Xã Thạnh Trị",
   "address": "Xã Thạnh Trị, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.0205667,
   "Latitude": 105.232624
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Xã Tân Thành",
   "address": "Ấp Tân Tiến, Xã Tân Thành, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1552253,
   "Latitude": 105.1845354
 },
 {
   "STT": 125,
   "Name": "Trạm Y tế Xã Tân An",
   "address": "Xã Tân An, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1176815,
   "Latitude": 105.193579
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Xã Lại Sơn",
   "address": "Xã Lại Sơn, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.8086251,
   "Latitude": 104.6412925
 },
 {
   "STT": 127,
   "Name": "Trạm y tế Xã Bình An",
   "address": "Xã Bình An, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.86210659999999,
   "Latitude": 105.1434387
 },
 {
   "STT": 128,
   "Name": "Trạm y tế Xã Giục Tượng",
   "address": "Xã Giục Tượng, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.9586817,
   "Latitude": 105.1668326
 },
 {
   "STT": 129,
   "Name": "Trạm y tế Xã Minh Hòa",
   "address": "Xã Minh Hòa, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.8754252,
   "Latitude": 105.2077798
 },
 {
   "STT": 130,
   "Name": "Trạm y tế Xã Mong Thọ A",
   "address": "Xã Mong Thọ A, Huyện Châu Thành, Tỉnh Kiên Giang ",
   "Longtitude": 10.0450276,
   "Latitude": 105.1785307
 },
 {
   "STT": 131,
   "Name": "Trạm y tế Xã Mong Thọ B",
   "address": "Xã Mong Thọ B, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.9679141,
   "Latitude": 105.2019295
 },
 {
   "STT": 132,
   "Name": "Trạm y tế Xã Thạnh Lộc",
   "address": "Xã Thạnh Lộc, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 10.0089019,
   "Latitude": 105.1375908
 },
 {
   "STT": 133,
   "Name": "Trạm y tế Xã Vĩnh Hòa Hiệp",
   "address": "Xã Vĩnh Hòa Hiệp, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.9277341,
   "Latitude": 105.1317431
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Xã Vĩnh Hòa Phú",
   "address": "Xã Vĩnh Hòa Phú, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 9.8957161,
   "Latitude": 105.1200482
 },
 {
   "STT": 135,
   "Name": "Trạm y tế Xã Mong Thọ",
   "address": "Xã Mong Thọ, Huyện Châu Thành, Tỉnh Kiên Giang",
   "Longtitude": 10.0216637,
   "Latitude": 105.2136302
 },
 {
   "STT": 136,
   "Name": "Trạm y tế Xã Bình An",
   "address": "Xã Bình An, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.1641051,
   "Latitude": 104.6004911
 },
 {
   "STT": 137,
   "Name": "Trạm y tế Xã Dương Hòa",
   "address": "Xã Dương Hòa, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.2972046,
   "Latitude": 104.5538744
 },
 {
   "STT": 138,
   "Name": "Trạm y tế Xã Hòa Điền",
   "address": "Xã Hòa Điền, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.3480893,
   "Latitude": 104.6354631
 },
 {
   "STT": 139,
   "Name": "Trạm y tế Xã Phú Mỹ",
   "address": " Xã Phú Mỹ , Huyện Kiên Lương, Kiên Giang",
   "Longtitude": 10.4377187,
   "Latitude": 104.5888356
 },
 {
   "STT": 140,
   "Name": "Trạm y tế Xã Tân khánh Hòa",
   "address": "Xã Tân Khánh Hoà, Huyện Giang Thành,  Tỉnh Kiên Giang",
   "Longtitude": 10.4897169,
   "Latitude": 104.6471221
 },
 {
   "STT": 141,
   "Name": "Trạm y tế Xã Vỉnh Điều",
   "address": "Xã Vĩnh Điều, Huyện Giang Thành, Tỉnh Kiên Giang",
   "Longtitude": 10.4749466,
   "Latitude": 104.7287607
 },
 {
   "STT": 142,
   "Name": "Trạm y tế Xã Sơn Hải",
   "address": "Xã Sơn Hải, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.1246809,
   "Latitude": 104.5072722
 },
 {
   "STT": 143,
   "Name": "Trạm y tế Xã Hòn Nghệ",
   "address": "Xã Hòn Nghệ, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.0193417,
   "Latitude": 104.5480484
 },
 {
   "STT": 144,
   "Name": "Trạm y tế Xã Kiên Bình",
   "address": " Xã Kiên Bình, Huyện Kiên Lương, Tỉnh kiên Giang",
   "Longtitude": 10.3437706,
   "Latitude": 104.7287607
 },
 {
   "STT": 145,
   "Name": "Trạm y tế Xã Vĩnh Phú",
   "address": "Xã Vĩnh Phú ,Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.8215443,
   "Latitude": 105.3189726
 },
 {
   "STT": 146,
   "Name": "Trạm y tế Xã Phú Lợi",
   "address": "Xã Phú Lợi ,Huyện Giang Thành , Tỉnh Kiên Giang",
   "Longtitude": 10.4377187,
   "Latitude": 104.5888356
 },
 {
   "STT": 147,
   "Name": "Trạm y tế Xã Bàn Tân Định",
   "address": "Xã Bàn Tân Định, Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.9400449,
   "Latitude": 105.259235
 },
 {
   "STT": 148,
   "Name": "Trạm y tế Xã Hoà An",
   "address": "Xã Hòa An, Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.9079262,
   "Latitude": 105.4419546
 },
 {
   "STT": 149,
   "Name": "Trạm y tế Xã Hoà Hưng",
   "address": "Xã Hòa Hưng ,Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.8634946,
   "Latitude": 105.4653897
 },
 {
   "STT": 150,
   "Name": "Trạm y tế Xã Hoà Lợi",
   "address": "Xã Hòa Lợi, Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.9274013,
   "Latitude": 105.488828
 },
 {
   "STT": 151,
   "Name": "Trạm y tế Xã Hoà Thuận",
   "address": "Xã Hòa Thuận, Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.8451045,
   "Latitude": 105.3950939
 },
 {
   "STT": 152,
   "Name": "Trạm y tế Xã Long Thạnh",
   "address": "Xã Long Thạnh, Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.8464431,
   "Latitude": 105.248737
 },
 {
   "STT": 153,
   "Name": "Trạm y tế Xã Ngọc Chúc",
   "address": "Xã Ngọc Chúc, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8849284,
   "Latitude": 105.3541013
 },
 {
   "STT": 154,
   "Name": "Trạm y tế Xã Thạnh Hòa",
   "address": "Xã Thạnh Hòa ,Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.9578656,
   "Latitude": 105.3014109
 },
 {
   "STT": 155,
   "Name": "Trạm y tế Xã Thạnh Hưng",
   "address": "Xã Thạnh Hưng, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.9556799,
   "Latitude": 105.348246
 },
 {
   "STT": 156,
   "Name": "Trạm y tế Xã Thạnh Lộc",
   "address": "Xã Thạnh Lộc ,Huyện Giồng Riềng ,Tỉnh Kiên Giang",
   "Longtitude": 9.9957759,
   "Latitude": 105.4185227
 },
 {
   "STT": 157,
   "Name": "Trạm y tế Xã Thạnh Phước",
   "address": "Xã Thạnh Phước, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 10.0186713,
   "Latitude": 105.3860797
 },
 {
   "STT": 158,
   "Name": "Trạm y tế Xã Vĩnh Thạnh",
   "address": "Xã Vĩnh Thạnh, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8638975,
   "Latitude": 105.3189064
 },
 {
   "STT": 159,
   "Name": "Trạm y tế Xã Ngọc Thuận",
   "address": "Xã Ngọc Thuận, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.95128699999999,
   "Latitude": 105.4419546
 },
 {
   "STT": 160,
   "Name": "Trạm y tế Xã Bàn Thạch",
   "address": "Xã Bàn Thạch, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8949566,
   "Latitude": 105.2545888
 },
 {
   "STT": 161,
   "Name": "Trạm y tế Xã Ngọc Thành",
   "address": "Xã Ngọc Thành, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.9163572,
   "Latitude": 105.3775245
 },
 {
   "STT": 162,
   "Name": "Trạm y tế Xã Hiệp Lộc",
   "address": "Xã Hiệp Lộc, Huyện Giồng Giềng, Tỉnh Kiên Giang",
   "Longtitude": 9.92327519999999,
   "Latitude": 105.4941353
 },
 {
   "STT": 163,
   "Name": "Trạm y tế Xã Ngọc Hoà",
   "address": "Xã Ngọc Hòa, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8529751,
   "Latitude": 105.416607
 },
 {
   "STT": 164,
   "Name": "Trạm y tế Xã Vĩnh Phú",
   "address": "Xã Vĩnh Phú, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8215443,
   "Latitude": 105.3189726
 },
 {
   "STT": 165,
   "Name": "Trạm y tế Xã Định An",
   "address": "Xã Định An, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.7833747,
   "Latitude": 105.3248269
 },
 {
   "STT": 166,
   "Name": "Trạm y tế Xã Định Hòa",
   "address": "Xã Định Hòa, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.78659049999999,
   "Latitude": 105.2545888
 },
 {
   "STT": 167,
   "Name": "Trạm y tế Xã Thới Quản",
   "address": "Xã Thới Quản,  Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.7908537,
   "Latitude": 105.1609838
 },
 {
   "STT": 168,
   "Name": "Trạm y tế Xã Thủy Liễu",
   "address": "Xã Thủy Liễu,  Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.76706499999999,
   "Latitude": 105.2077798
 },
 {
   "STT": 169,
   "Name": "Trạm y tế Vĩnh Hòa Hưng Bắc",
   "address": "Xã Vĩnh Hòa Hưng Nam,Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.743616,
   "Latitude": 105.3658125
 },
 {
   "STT": 170,
   "Name": "Trạm y tế Vĩnh Hòa Hưng Nam",
   "address": "Xã Vĩnh Hòa Hưng Bắc, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.8028724,
   "Latitude": 105.3716684
 },
 {
   "STT": 171,
   "Name": "Trạm y tế Xã Vĩnh Phước A",
   "address": "Xã Vĩnh Phước A, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.6932656,
   "Latitude": 105.2838511
 },
 {
   "STT": 172,
   "Name": "Trạm y tế Xã Vĩnh Phước B",
   "address": "Xã Vĩnh Phước B, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.7195109,
   "Latitude": 105.3014109
 },
 {
   "STT": 173,
   "Name": "Trạm y tế Xã Vĩnh Tuy",
   "address": "Xã Vĩnh Tuy, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.6246931,
   "Latitude": 105.3658125
 },
 {
   "STT": 174,
   "Name": "Trạm y tế Xã Vĩnh Thắng",
   "address": "Xã Vĩnh Thắng, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.6478956,
   "Latitude": 105.3306814
 },
 {
   "STT": 175,
   "Name": "Trạm y tế Xã Hưng Yên",
   "address": "Xã Hưng Yên, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.8146501,
   "Latitude": 105.1142011
 },
 {
   "STT": 176,
   "Name": "Trạm y tế Xã Nam Thái",
   "address": "Xã Nam Thái, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.8416255,
   "Latitude": 104.9973028
 },
 {
   "STT": 177,
   "Name": "Trạm y tế Xã Nam Thái A",
   "address": "Xã Nam thái A, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.822034,
   "Latitude": 104.9505671
 },
 {
   "STT": 178,
   "Name": "Trạm y tế Xã Nam Yên",
   "address": "Xã Nam Yên, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.8612066,
   "Latitude": 105.044052
 },
 {
   "STT": 179,
   "Name": "Trạm y tế Xã Đông Thái",
   "address": "Xã Đông Thái, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.77551699999999,
   "Latitude": 105.0206757
 },
 {
   "STT": 180,
   "Name": "Trạm y tế Xã Đông Yên",
   "address": "Xã Đông Yên, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.7506944,
   "Latitude": 105.0908147
 },
 {
   "STT": 181,
   "Name": "Trạm y tế Xã Tây Yên",
   "address": "Xã Tây Yên, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.92525339999999,
   "Latitude": 105.0674317
 },
 {
   "STT": 182,
   "Name": "Trạm y tế Xã Tây Yên A",
   "address": "Xã Tây yên A, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.87561949999999,
   "Latitude": 105.0849687
 },
 {
   "STT": 183,
   "Name": "Trạm y tế Xã Thạnh Yên",
   "address": "Xã Thạnh Yên ,Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.70527959999999,
   "Latitude": 105.1375908
 },
 {
   "STT": 184,
   "Name": "Trạm y tế Xã Thạnh Yên A",
   "address": "Xã Thạnh Yên A, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.70527959999999,
   "Latitude": 105.1375908
 },
 {
   "STT": 185,
   "Name": "Trạm y tế Xã An Minh Bắc",
   "address": "Xã An Minh Bắc, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.62081759999999,
   "Latitude": 105.0908147
 },
 {
   "STT": 186,
   "Name": "Trạm Y tế Xã  Đông Hưng",
   "address": "Xã Đông Hưng, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.6259911,
   "Latitude": 104.9739333
 },
 {
   "STT": 187,
   "Name": "Trạm Y tế Xã  Đông Hưng A",
   "address": " Xã Đông Hưng A, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.6734083,
   "Latitude": 104.8804893
 },
 {
   "STT": 188,
   "Name": "Trạm Y tế Xã  Đông Hưng B",
   "address": "Xã Đông Hưng B, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.5827226,
   "Latitude": 104.9739333
 },
 {
   "STT": 189,
   "Name": "Trạm Y tế Xã Đông Hòa",
   "address": " Xã Đông Hòa, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.71155729999999,
   "Latitude": 104.9973028
 },
 {
   "STT": 190,
   "Name": "Trạm Y tế Xã  Đông Thạnh",
   "address": " Xã Đông Thạnh, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.6692828,
   "Latitude": 104.9739333
 },
 {
   "STT": 191,
   "Name": "Trạm Y tế Xã Thuận Hòa",
   "address": "Xã Thuận Hòa, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.7807426,
   "Latitude": 104.9038451
 },
 {
   "STT": 192,
   "Name": "Trạm Y tế Xã Vân Khánh",
   "address": " Xã Vân Khánh, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.5868126,
   "Latitude": 104.8804893
 },
 {
   "STT": 193,
   "Name": "Trạm Y tế Xã Vân Khánh Đông",
   "address": " Xã Vân Khánh Đông, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.6300989,
   "Latitude": 104.8804893
 },
 {
   "STT": 194,
   "Name": "Trạm Y tế Xã Vân Khánh Tây",
   "address": " Xã Vân Khánh Tây, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.5435495,
   "Latitude": 104.8804893
 }
];
